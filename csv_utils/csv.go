package csv_utils

import (
	"encoding/csv"
	"log"
	"os"
)

func ReadAsCSV(fileName string) (*csv.Reader, error) {
	csvfile, err := os.Open("NIFTY.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
		return nil, err
	}
	// Parse the file
	return csv.NewReader(csvfile), nil
}
