package config

type Stock struct {
	Name  string
	High  float64
	Value float64
	Low   float64
}

// "Symbol ","Open ","High ","Low ",
// "PREV. CLOSE ","LTP ","chng ","%Chng ",
// "Volume (shares)","Value ","52W H ","52W L "

// Open
// 	High
// 	Low
// 	LTP
// 	Value
