package main

import (
	"fmt"
	"io"
	"log"
	"strconv"

	"gitlab.com/shubhashree/practise/stock/analytics"
	"gitlab.com/shubhashree/practise/stock/config"
	"gitlab.com/shubhashree/practise/stock/csv_utils"
)

func main() {

	// var stocksInfo map[string]string

	stockInfo := make([]config.Stock, 0)

	// stocksInfo := make(map[string][]string)
	r, err := csv_utils.ReadAsCSV("NIFTY.csv")
	if err != nil {
		log.Panic(err)
	}
	cnt := 0
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		if cnt == 0 {
			cnt++
			continue
		}
		cnt++

		tmp2, _ := strconv.ParseFloat(record[5], 32)
		tmp3, _ := strconv.ParseFloat(record[10], 32)
		tmp4, _ := strconv.ParseFloat(record[11], 32)

		stockInfo = append(stockInfo, config.Stock{
			Name:  record[0],
			Value: tmp2,
			High:  tmp3,
			Low:   tmp4,
		})
		// stocksInfo[record[0]] = []string{record[1], record[2], record[3]}

		fmt.Printf("Stock: %s \n", record)

		// record[1], record[2]
		// for i, s := range record {
		// 	fmt.Println(i, s)
		// }
	}

	//jsonData, _ := json.Marshal(stockInfo)
	//fmt.Printf("Stock: %s \n", string(jsonData))
	analytics.GetLow(stockInfo)
	analytics.Get52WeekHigh(stockInfo)

}
