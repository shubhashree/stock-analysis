package analytics

import (
	"fmt"

	"gitlab.com/shubhashree/practise/stock/config"
)

func GetLow(stocksInfo []config.Stock) []string {
	cnt := 0
	for _, stock := range stocksInfo {
		var percentLow = (stock.Value - stock.Low) / stock.Value * 100
		if percentLow <= 10 {
			fmt.Println("The stocks 5 % around 52 week low : ", stock.Name, percentLow)
			cnt++
		}
	}
	fmt.Println("The number of stocks near 52 week low : ", cnt)
	return []string{}
}

func Get52WeekHigh(stocksInfo []config.Stock) []string {
	cnt := 0
	for _, stock := range stocksInfo {
		var percentHigh = (stock.High - stock.Value) / stock.High * 100
		if percentHigh <= 5 {
			fmt.Println("The stocks 5 % around 52 week high: ", stock.Name, percentHigh)
			cnt++
		}
	}
	fmt.Println("The number of stocks near 52 week high : ", cnt)
	return []string{}
}
